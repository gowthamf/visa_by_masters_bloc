import 'dart:convert';

import 'package:http/http.dart' show Client;
import 'package:visa_by_masters_common/src/models/visa_details.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';

class VisaDetailsApiProvider{
  Client client=Client();

  Future<VisaDetails> getVisaDetails(int visaReqId)async{
    final response=await client.get('${ConstantsValues().baseUrl}/VisaDetails/GetGeneralInfo/$visaReqId');

    if(response.statusCode==200){
      var visaDetails=json.decode(response.body);

      return VisaDetails.fromJson(visaDetails[0]);
    } else if(response.statusCode==204){
      print('No Visa Details Available');
      return VisaDetails();
    } else{
      print(
          'Connection problem, Please check your Internet and refresh the page');
      return VisaDetails();
    }
  }
}