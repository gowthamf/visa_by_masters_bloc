import 'package:http/http.dart' show Client;
import 'package:visa_by_masters_common/src/resources/constantsvalues.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'dart:async';
import 'dart:convert';

class CountryApiProvider {
  Client client = Client();

  Future<List<Country>> getCountries() async {
    final response =
        await client.get('${ConstantsValues().baseUrl}/Country/GetCountires');

    List<Country> _countryList = [];

    if (response.statusCode == 200) {
      List<dynamic> countryList = json.decode(response.body);
      for (var i = 0; i < countryList.length; i++) {
        _countryList.add(Country.fromJson(countryList[i]));
      }
      return _countryList;
    } else {
      print('No Data for Countries');
      return _countryList;
    }
  }

  Future<List<Country>> getDefaultCountries() async {
    final response = await client.get('assets/Countries.json');

    List<Country> _countryList = [];

    if (response.statusCode == 200) {
      List<dynamic> countryList = json.decode(response.body);
      for (var i = 0; i < countryList.length; i++) {
        _countryList.add(Country.defaultFromJson(countryList[i]));
      }
      return _countryList;
    } else {
      print('No Data for Countries');
      return _countryList;
    }
  }
}
