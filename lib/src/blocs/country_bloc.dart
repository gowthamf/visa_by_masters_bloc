import 'package:rxdart/rxdart.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';

class CountryBloc {
  final Repository _repo = Repository();
  BehaviorSubject<List<Country>> _countryListFetcher =
      BehaviorSubject<List<Country>>();

  final BehaviorSubject<List<Country>> _deafultCountryListFetcher =
      BehaviorSubject<List<Country>>();

  BehaviorSubject<List<Country>> _filteredCountryList = BehaviorSubject<List<Country>>();  

  Stream<List<Country>> get countryList => _countryListFetcher.stream;

  Stream<List<Country>> get defaultCountryList =>
      _deafultCountryListFetcher.stream;

  Stream<List<Country>> get filteredCountryList => _filteredCountryList.stream;    

  getCountries() async {
    List<Country> country = await _repo.getCountryList();
    _countryListFetcher.sink.add(country);
  }

  getDefaultCountries() async {
    List<Country> country = await _repo.getDefaultCountries();
    _deafultCountryListFetcher.sink.add(country);
  }

  getContinentCountryList(int continentId) {
    List<Country> continentCountry= List<Country>();
    _filteredCountryList= BehaviorSubject<List<Country>>();
    countryList.listen((onData)=>{
      continentCountry = onData.where((c)=>c.continentId==continentId).toList(),
      _filteredCountryList.sink.add(continentCountry),
    });
  }

  getPopularCountriesList(){
    List<Country> popularCountry = List<Country>();
    _filteredCountryList = BehaviorSubject<List<Country>>();

    countryList.listen((onData)=>{
      popularCountry= onData.where((c)=>c.isPopularVisa== true).toList(),
      _filteredCountryList.sink.add(popularCountry),
    });
  }

  dispose() {
    _countryListFetcher.close();
  }
}

final CountryBloc countryBloc = CountryBloc();
