import 'package:rxdart/rxdart.dart';
import 'package:visa_by_masters_common/src/models/visa.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';

class VisaBloc {
  final Repository _repo = Repository();
  final PublishSubject<Visa> _visaReqFetcher = PublishSubject<Visa>();

  Stream<Visa> get visaRequirements => _visaReqFetcher.stream;

  getVisaRequirement(int fromCountryId, int toCountryId) async {
    Visa visa = await _repo.getVisaRequirements(fromCountryId, toCountryId);
    _visaReqFetcher.sink.add(visa);
  }

  dispose() {
    _visaReqFetcher.close();
  }
}

final VisaBloc visaBloc = VisaBloc();
