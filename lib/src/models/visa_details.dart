class VisaDetails {
  final int id;
  final int visareqid;
  final VisaInfo visainfo;
  final ContactDetails contactdetails;

  int get detailsId => id;
  int get visaReqId => visareqid;
  VisaInfo get visaInfo => visainfo;
  ContactDetails get contactDetails => contactdetails;

  VisaDetails({this.id, this.visareqid, this.visainfo, this.contactdetails});

  factory VisaDetails.fromJson(Map<String, dynamic> json) {
    return VisaDetails(
        id: json['id'],
        visareqid: json['visaRequirementId'],
        visainfo: VisaInfo.fromJson(json['visaInfo']),
        contactdetails: ContactDetails.fromJson(json['contactDetails']));
  }
}

class VisaInfo {
  final TouristInfo touristinfo;
  final BusinessInfo businessinfo;
  final List<dynamic> generalinfo;

  TouristInfo get touristInfo => touristinfo;
  BusinessInfo get businessInfo => businessinfo;
  List<dynamic> get generalInfo => generalinfo;

  VisaInfo({this.businessinfo, this.touristinfo, this.generalinfo});

  factory VisaInfo.fromJson(Map<String, dynamic> json) {
    return VisaInfo(
        generalinfo: json['generalInformation'],
        businessinfo: BusinessInfo.fromJson(json['businessInfo']),
        touristinfo: TouristInfo.fromJson(json['touristInfo']));
  }
}

class TouristInfo {
  final List<dynamic> maindocuments;
  final List<dynamic> sponsorsdocuments;

  List<dynamic> get mainDocuments => maindocuments;
  List<dynamic> get sponsorsDocuments => sponsorsdocuments;

  TouristInfo({this.maindocuments, this.sponsorsdocuments});

  factory TouristInfo.fromJson(Map<String, dynamic> json) {
    return TouristInfo(
        maindocuments: json['mainDocuments'],
        sponsorsdocuments: json['sponsorsDocuments']);
  }
}

class BusinessInfo {
  final List<dynamic> maindocuments;
  final List<dynamic> sponsorsdocuments;

  List<dynamic> get mainDocuments => maindocuments;
  List<dynamic> get sponsorsDocuments => sponsorsdocuments;

  BusinessInfo({this.maindocuments, this.sponsorsdocuments});

  factory BusinessInfo.fromJson(Map<String, dynamic> json) {
    return BusinessInfo(
        maindocuments: json['mainDocuments'],
        sponsorsdocuments: json['sponsorsDocuments']);
  }
}

class ContactDetails {
  final String embassyname;
  final List<dynamic> embassycontactdetails;
  final String visacentername;
  final List<dynamic> visacentercontactdetails;

  String get embassyName => embassyname;
  List<dynamic> get embassyContactDetails => embassycontactdetails;
  String get visaCenterName => visacentername;
  List<dynamic> get visaCenterContactDetails => visacentercontactdetails;

  ContactDetails(
      {this.embassyname,
      this.embassycontactdetails,
      this.visacentername,
      this.visacentercontactdetails});

  factory ContactDetails.fromJson(Map<String, dynamic> json) {
    return ContactDetails(
        visacentername: json['visaCenterName'],
        embassycontactdetails: json['embassyContactDetails'],
        embassyname: json['embassyName'],
        visacentercontactdetails: json['visaCenterContactDetails']);
  }
}
