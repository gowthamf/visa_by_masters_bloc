class Country {
  final int id;
  final String countryname;
  final String countryFlagPath;
  final bool ispopularvisa;
  final int continentid;

  int get countryId => id;
  String get countryName => countryname;
  String get countryFlag => countryFlagPath;
  bool get isPopularVisa => ispopularvisa;
  int get continentId => continentid;

  Country(
      {this.id, this.countryname, this.countryFlagPath, this.ispopularvisa, this.continentid});

  factory Country.fromJson(Map<String, dynamic> json) {
    return Country(
        id: json['id'],
        countryname: json['countryName'],
        countryFlagPath: 'assets/images/countries/${json['countryFlagPath']}',
        ispopularvisa: json['isPopularVisa'],
        continentid: json['continentId']
        );
  }

  factory Country.defaultFromJson(Map<String, dynamic> json) {
    return Country(
      countryname: json['CountryName'],
      countryFlagPath: 'assets/images/countries/${json['countryFlagPath']}',
    );
  }
}
