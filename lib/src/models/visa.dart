import 'package:visa_by_masters_common/visa_by_masters_common.dart';

class Visa {
  final int visaid;
  final int fromcountryid;
  final int tocountryid;
  final bool isvisarequired;
  final bool isvisaonarrival;
  final bool isvisafree;
  final String countryname;
  final Country fromcountry;

  int get visaId=>visaid;
  int get fromCountryId => fromcountryid;
  int get toCountryId => tocountryid;
  bool get isVisaRequired => isvisarequired;
  bool get isVisaOnArrival => isvisaonarrival;
  bool get isVisaFree => isvisafree;
  String get countryName => countryname;
  Country get fromCountry => fromcountry;

  Visa(
      {this.visaid,
      this.fromcountryid,
      this.countryname,
      this.isvisafree,
      this.isvisaonarrival,
      this.isvisarequired,
      this.tocountryid,
      this.fromcountry});

  factory Visa.fromJson(Map<String, dynamic> json) {
    return Visa(
        visaid: json['id'],
        tocountryid: json['toCountryId'],
        fromcountryid: json['fromCountryId'],
        isvisafree: json['isVisaFree'],
        isvisaonarrival: json['isVisaOnArrival'],
        isvisarequired: json['isVisaRequired'],
        fromcountry: Country.fromJson(json['fromCountry']));
  }
}
