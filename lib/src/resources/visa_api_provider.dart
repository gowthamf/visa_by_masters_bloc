import 'package:http/http.dart' show Client;
import 'package:visa_by_masters_common/src/models/visa.dart';
import 'package:visa_by_masters_common/src/resources/constantsvalues.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'dart:async';
import 'dart:convert';

class VisaApiProvider {
  Client client = Client();

  Future<Visa> getVisaRequirements(int fromCountryId, int toCountryId) async {
    final response = await client.get(
        '${ConstantsValues().baseUrl}/VisaRequirement/GetCountryVisaRequirements/$toCountryId/$fromCountryId');

    if (response.statusCode == 200) {
      var visa = json.decode(response.body);

      return Visa.fromJson(visa);
    } else if (response.statusCode == 204) {
      print('No Visa Details Available');
      return Visa();
    } else {
      print(
          'Connection problem, Please check your Internet and refresh the page');
      return Visa();
    }
  }
}
