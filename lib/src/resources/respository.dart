import 'dart:async';

import 'package:visa_by_masters_common/src/models/visa.dart';
import 'package:visa_by_masters_common/src/models/visa_details.dart';
import 'package:visa_by_masters_common/src/resources/visa_api_provider.dart';
import 'package:visa_by_masters_common/src/resources/visa_details_api_provider.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';

class Repository {
  final CountryApiProvider countryApiProvider = CountryApiProvider();
  final VisaApiProvider visaApiProvider = VisaApiProvider();
  final VisaDetailsApiProvider visaDetailsApiProvider =
      VisaDetailsApiProvider();

  Future<List<Country>> getCountryList() => countryApiProvider.getCountries();
  Future<List<Country>> getDefaultCountries() =>
      countryApiProvider.getDefaultCountries();

  Future<Visa> getVisaRequirements(int fromCountryId, int toCountryId) =>
      visaApiProvider.getVisaRequirements(fromCountryId, toCountryId);
  Future<VisaDetails> getVisaDetails(int visaReqId) =>
      visaDetailsApiProvider.getVisaDetails(visaReqId);
}
