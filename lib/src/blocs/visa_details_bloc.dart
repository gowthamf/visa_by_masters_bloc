import 'package:rxdart/rxdart.dart';
import 'package:visa_by_masters_common/src/models/visa_details.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';

class VisaDetailsBloc {
  final Repository _repo = Repository();
  final PublishSubject<VisaDetails> _visaDetailsFetcher =
      PublishSubject<VisaDetails>();

  Stream<VisaDetails> get visaDetails => _visaDetailsFetcher.stream;

  getVisaDetails(int visaReqId) async {
    VisaDetails visaDetails = await _repo.getVisaDetails(visaReqId);
    _visaDetailsFetcher.sink.add(visaDetails);
  }

  dispose() {
    _visaDetailsFetcher.close();
  }
}

final VisaDetailsBloc visaDetailsBloc = VisaDetailsBloc();
